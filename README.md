# 요구사항 : 여러 영어 문장에 대한 그룹을 지을 수 있는 템플릿을 등록한다.


[요구사항 명세 in Figma](https://www.figma.com/file/aJc37K7V1gEvmRO0et70Ni/English-Sentences-Template?node-id=1125%3A1888)

<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FaJc37K7V1gEvmRO0et70Ni%2FEnglish-Sentences-Template%3Fnode-id%3D1125%253A1888" allowfullscreen></iframe>

![요구사항명세](./docs/Requirements_preview.png)